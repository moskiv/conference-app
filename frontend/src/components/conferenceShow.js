import React, { Component, Fragment } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

class conferenceShow extends Component {
state ={
      conference: [],
      people: [],
  }

  componentDidMount() {
    axios.get('/conference/'+this.props.match.params.id)
      .then(res => {
        this.setState({ conference: res.data });
        console.log("THIS IS CONFERENCE" + this.state.conference);
      });
  }

  delete(id){
    console.log(id);
    axios.delete('/conference/'+id)
      .then((result) => {
        this.props.history.push("/")
      });
  }

  render() {
  const {conference, room } = this.state;
    return (
      <div className="container">
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">
              Conference Details
            </h3>
          </div>
          <div class="panel-body">
            <button class="btn btn-info"><Link to="/" style={{ textDecoration: 'none' , color: 'white'}} ><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Conference List</Link></button>
            <dl id="showBody">
              <dt>Name:</dt>
              <dd>{this.state.conference.name}</dd>
              <dt>Room:</dt>
              <dd>{this.state.conference.croom}</dd>
              <dt>Participants:</dt>
              <dd>{this.state.conference.people}</dd>
                </dl>
                <button id="delConf" onClick={this.delete.bind(this, this.state.conference.id)} class="btn btn-danger">Delete Conference</button>
          </div>
        </div>
      </div>
    );
  }
}

export default conferenceShow;