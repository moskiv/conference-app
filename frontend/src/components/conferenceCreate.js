import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';


class conferenceCreate extends Component {

  constructor() {
    super();
    this.state = {
      name: '',
      person: '',
      room: [],
      croom:'',
      item:'',
      people: [],
      currentItem: {text:'', key:''},
    };
  }

    componentDidMount() {
    axios.get('/room')
    .then(res  => {
    this.setState({room: res.data});
    });
    }

    onChange = (a) => {
    console.log(a.target.value)
    const state = this.state
    state[a.target.name] = a.target.value;
    this.setState(state);
  }

   onSubmit = (a) => {
   a.preventDefault();
       const {name, person, croom, people} = this.state;
       axios.post('/conference', {name, person, croom, people })
       .then((result) => {
      this.props.history.push("/")
       });
       }

    onPersonSubmit = (event) => {
    event.preventDefault();
    this.setState({
      item: '',
      people: [...this.state.people, this.state.item]
    });
    }

    onPersonChange = (event) => {
    console.log(event.target.value)
    this.setState({ item: event.target.value });
    }

    delete(c){
    c.preventDefault();
    console.log(c);
            axios.delete('/conference/'+c.id)
              .then((res) => {
                this.props.history.push("/")
                this.setState({ state: this.state });
              });
          }

    handlePersonRemove(item){
    console.log(item)
    this.setState(prevState => ({
              people: prevState.people.filter(el => el != item)
          }));
        }

  render() {
    const { name, person, croom, people, item} = this.state;

    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Add new Conference
            </h3>
          </div>
          <div class="panel-body">

            <button class="btn btn-info"><Link to="/" style={{ textDecoration: 'none' , color: 'white'}} ><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Conference List</Link></button>
             <form onSubmit={this.onSubmit}>
              <div class="form-group">
                <label for="name">Conference Name:</label>
                <input type="text" class="form-control" name="name" value={name} onChange={this.onChange} placeholder="Enter any name" />
              </div>

                <div>
                  <label for="croom">Choose a Room:</label>
                  <select name="croom" onChange={this.onChange}>
                  {this.state.room.map(({id, name, seats}) => (
                  <option key={id}>{name}</option>
                  ))}
                  </select>
                </div>
                <button id="submitbtn" type="submit" class="btn btn-primary">Submit form</button>
              </form>
          </div>
        </div>
               <div>
                <form className="App" onSubmit={this.onPersonSubmit}>
                  <input name='people' value={this.state.item} onChange={this.onPersonChange} />
                  <button class="btn btn-success">Add a person</button>
                </form>
              </div>
        <table class="table table-stripe">
        <thead>
          <tr>
            <th>People</th>
          </tr>
        </thead>
        <tbody>
          {this.state.people.map((item, id) => (
            <tr>
              <td key={item}>{item}</td>
            <td id="tdDelBtn"><button onClick={this.handlePersonRemove.bind(this, item )} class="btn btn-danger">Delete</button></td>
            </tr>
          ))}
        </tbody>
      </table>
</div>

    );
  }
}

export default conferenceCreate;