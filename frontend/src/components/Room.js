import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';

class Room extends Component {
  constructor(state) {
    super(state);
    this.state = {
      room: [],
    };
  }

componentDidMount() {
  axios.get('/room')
    .then(res  => {
    this.setState({room: res.data});
    });
    }


  render() {
    return (
 <div className="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Room Details
            </h3>
          </div>
          <div class="panel-body">
            <button class="btn btn-info"><Link to="/" style={{ textDecoration: 'none' , color: 'white'}} ><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Conference List</Link></button>
              <table class="table">
              <thead class="thead-dark">
                <tr >
                  <th class=".col-xs-6 .col-md-2">Room name</th>
                  <th class=".col-xs-6 .col-md-2">Max seats</th>
                </tr>
              </thead>
              <tbody>
                {this.state.room.map(r => (
                  <tr>
                    <td key={r.id}>{r.name}</td>
                    <td>{r.seats}</td>
                  </tr>
                ))}
            </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default Room;