import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import './App.css';
import conferenceCreate from './components/conferenceCreate';
import conferenceShow from './components/conferenceShow';
import Room from './components/Room';




ReactDOM.render(
  <Router>
      <div>
        <Route exact path='/' component={App} />
        <Route path='/conferenceCreate' component={conferenceCreate} />
        <Route path='/conferenceShow/:id' component={conferenceShow} />
        <Route path='/Room/' component={Room} />
      </div>
  </Router>,
  document.getElementById('root')
);


/*import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
*/