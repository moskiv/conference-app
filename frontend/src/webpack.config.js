const path = require('path');

module.exports = {
  entry: './src/components/App.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'build')
  },

devtool: '#sourcemap',

  module: {
        loaders: [
        {
            test: /\.jsx?$/,
            exclude:/(node_modules)/,
            use:[
                {
                    loader: 'babel-loader',
                    options: { presets: ["env", "react"] }
                }
            ]
        },
        {
        test: /\.css$/,
        loader: 'style-loader!css-loader' },
        ]
  }
};