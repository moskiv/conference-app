import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';


class App extends Component {
 constructor(state) {
    super(state);
    this.state = {
      conference: [],
      room: [],
    };
  }

 componentDidMount() {
 Promise.all([
 axios.get('/conference'),
 axios.get('/room')
 ]).then(([conferenceRepo, roomRepo]) => {
    this.setState({conference: conferenceRepo.data, room: roomRepo.data});
    });
 }

delete(c){
    console.log(c);
    axios.delete('/conference/'+c.id)
    .then((res) => {
      this.props.history.push("/")
      this.setState({ state: this.state });
    });
    }

handleDelete(id){
    console.log(id);
    axios.delete('/conference/'+id)
      .then((res) => {
        this.props.history.push("/")
      });
  }


render() {
const {conference, room} = this.state
return (
  <div className="container">
    <div className="panel panel-default">
      <div className="panel-heading">
        <h3 className="panel-title">
          Conference list
        </h3>
      </div>
      <div class="panel-body">
        <table class="table table-stripe">
          <thead class="thead-dark">
            <tr>
              <th>Name</th>
              <th>Room</th>
            </tr>
          </thead>
          <tbody>
            {this.state.conference.map((c) => (
              <tr>
                <td key={c.id}><Link to={`/conferenceShow/${c.id}`}>{c.name}</Link></td>
                <td>{c.croom}</td>
              <button onClick={this.delete.bind(this, c)} class="btn btn-danger">Delete</button>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
<button id="roomInfo" class="btn btn-info"><Link to={`/Room/`} style={{ textDecoration: 'none' , color: 'white'}} >Rooms</Link></button>
<h4><Link to='/conferenceCreate'><button style={{position: 'right'}} class="btn btn-success"> Add new conference</button> </Link></h4>
  </div>
    );
  }
}

export default App;
