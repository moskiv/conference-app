package com.Tallink.Conference.controllers;

import com.Tallink.Conference.models.Room;
import com.Tallink.Conference.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;

@RestController
public class RoomController {

        @Autowired
        RoomRepository roomRepository;

        @RequestMapping(method=RequestMethod.GET, value="/room")
        public Iterable<Room> room() {
            return roomRepository.findAll();
        }

        @RequestMapping(method=RequestMethod.POST, value="/room")
        public Room save(@RequestBody Room room) {
            roomRepository.save(room);

            return room;
        }

        @RequestMapping(method=RequestMethod.GET, value="/room/{id}")
        public Optional<Room> show(@PathVariable String id) {
            return roomRepository.findById(id);
        }

        @RequestMapping(method=RequestMethod.PUT, value="/room/{id}")
        public Room update(@PathVariable String id, @RequestBody Room room) {
            Optional<Room> optroom = roomRepository.findById(id);
            Room r = optroom.get();
            if(room.getName() != null)
                r.setName(room.getName());

            return r;
        }

        @RequestMapping(method=RequestMethod.DELETE, value="/room/{id}")
        public String delete(@PathVariable String id) {
            Optional<Room> optcontact = roomRepository.findById(id);
            Room room = optcontact.get();
            roomRepository.delete(room);

            return "";
        }
    }

