package com.Tallink.Conference.controllers;

import com.Tallink.Conference.models.Conference;
import com.Tallink.Conference.repositories.ConferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.stream.Stream;

@RestController
public class ConferenceController {

    @Autowired
    ConferenceRepository ConferenceRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/conference")
    public Iterable<Conference> conference() {
        return ConferenceRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/conference")
    public Conference save(@RequestBody Conference conference) {
        ConferenceRepository.save(conference);

        return conference;
    }

    @RequestMapping(method=RequestMethod.GET, value="/conference/{id}")
    public Optional<Conference> show(@PathVariable String id) {
        return ConferenceRepository.findById(id);
    }



    @RequestMapping(method = RequestMethod.DELETE, value = "/conference/{id}")
    public String delete(@PathVariable String id) {
        Optional<Conference> optcontact = ConferenceRepository.findById(id);
        Conference conference = optcontact.get();
        ConferenceRepository.delete(conference);

        return "";
    }


}
