package com.Tallink.Conference.repositories;

import com.Tallink.Conference.models.Conference;
import org.springframework.data.repository.CrudRepository;

public interface ConferenceRepository extends CrudRepository<Conference, String> {
    @Override
    void delete(Conference deleted);
}
