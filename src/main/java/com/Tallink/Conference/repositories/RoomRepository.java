package com.Tallink.Conference.repositories;

import com.Tallink.Conference.models.Room;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoomRepository extends CrudRepository<Room, String> {
    @Override
    void delete(Room deleted);

    List<Room> findByName(String name);
}

