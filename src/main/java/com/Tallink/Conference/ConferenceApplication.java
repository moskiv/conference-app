package com.Tallink.Conference;

import com.Tallink.Conference.models.Conference;
import com.Tallink.Conference.models.Room;
import com.Tallink.Conference.repositories.ConferenceRepository;
import com.Tallink.Conference.repositories.RoomRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

import java.util.Arrays;


@SpringBootApplication
public class ConferenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConferenceApplication.class, args);
	}

	@Bean
	public CommandLineRunner getRoom(RoomRepository roomRepository) {
		return (args) -> {


			roomRepository.deleteAll();
			roomRepository.save(new Room("M/S Baltic Queen conference", 5));
			roomRepository.save(new Room("M/S Baltic King conference", 10));
			roomRepository.save(new Room("M/S Baltic Royal conference", 15));


			for (Room room : roomRepository.findAll()) {
				System.out.println(room.getName());
			}

		};
	}


	@Bean
	public CommandLineRunner getConference(ConferenceRepository conferenceRepository){
		return (args) -> {

			List<String> people = new ArrayList<>();
			people.add("guy");
			people.add("dog");
			people.add("viktor");
			conferenceRepository.save(new Conference( "guy","boy","nay", people));

			for (Conference conference : conferenceRepository.findAll()){
				System.out.println(conference.getPeople());
			}
		}; }


}

