package com.Tallink.Conference.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
@Document(collection = "room")
public class Room {

        @Id
        String id;
        String name;
        Integer seats;

        public Room() {
        }

        public Room(String name, int seats) {
            this.name = name;
            this.seats = seats;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getSeats() {
            return seats;
        }

        public void setSeats(int seats) {
            this.seats = seats;
        }


    @Override
    public String toString() {
        return String.format(
                "Room[id=%d, name='%s', seats='%d']",
                id, name, seats);
}
}
