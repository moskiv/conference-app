package com.Tallink.Conference.models;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "conference")
public class Conference {
    @Id
    String id;
    String name;
    String person;
    String croom;
    List<String> people;

    public Conference() {this.people = new ArrayList<>();}

    public Conference( String name, String person, String croom, List<String> people){

        this.id = id;
        this.name = name;
        this.person = person;
        this.croom = croom;
        this.people =  people;
    }

    public List<String> getPeople() {
        return people;
    }

    public void setPeople(List<String> people) {
        this.people = people;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getCroom() {
        return croom;
    }

    public void setCroom(String croom) {
        this.croom = croom;
    }
}
